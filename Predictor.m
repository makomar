classdef Predictor <handle
    properties 
        lastMusPos = [0,0;0,0];
        times = [0,0];
        speed = [0,0;0,0];
        accel = [0;0];
        lostTime =  1e10;
    end
    
    methods
        
        function obj = Predictor()
           
        end
        
        function obj = newData(obj, posX, posY, time)
           obj.lastMusPos(:,2) = obj.lastMusPos(:,1);
           obj.speed(:,2) = obj.speed(:,1);
           obj.times(2) = obj.times(1);
           obj.lastMusPos(1,1) = posX;
           obj.lastMusPos(2,1) = posY;
           obj.times(1) = time;
           obj.speed(:,1) = (obj.lastMusPos(:,1) - obj.lastMusPos(:,2)) ./ (obj.times(:,1) - obj.times(:,2));
           obj.accel = (obj.speed(:,1) - obj.speed(:,2)) ./ (obj.times(:,1) - obj.times(:,2));
        end
        
        %vrati pozici, kde ocekava komara za dany cas
        function [x,y] = predict(obj, time)
           deltaTime = double(time - obj.times(1));
            if deltaTime < obj.lostTime
%             test1 = ((obj.speed(:,1)  + obj.accel .* deltaTime).^2 - obj.speed(:,1).^2) .* deltaTime ./2
            P = obj.lastMusPos(:,1) + obj.speed(:,1) .* deltaTime;%((obj.speed(:,1)  + obj.accel .* deltaTime).^2 - obj.speed(:,1).^2) .* deltaTime ./2;
            else
             P = [0,0];
            end
           x = P(1);
           y = P(2);
        end
    end
end