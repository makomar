classdef CamDriver <handle
    %CAMDRIVER Trida pro ovladani motoru a ziskani polohz kamery 
    
    properties (SetAccess = private)
        ser; %odkaz na pouzite seriove rozhrani
        trans; %modul pro transformaci souradnic
        lastAZ = 127;
        lastEL = 127;
        readAZ = 127;
        readEL = 127;
        callbackTmp;
        sendState = 0;
        lastSpeed = 2;
    end
    
    
    methods (Access = private)
        function ptmove(obj, pan_pos, tilt_pos, speed)
            if nargin<4
                speed = 3;
            end

            obj.sermove( speed, 0, pan_pos);
            obj.sermove( speed, 1, tilt_pos);
        end

        function sermove(obj, speed, pantilt, pos)
            pantilt = round(pantilt);
            if pantilt > 255
                pantilt = 255;
            end
            if pantilt < 0
                pantilt = 0;
            end
            b1 = uint8(hex2dec('ff'));
            b2 = uint8(bitshift(uint8(speed), 5) + uint8(pantilt));
            b3 = min(max(uint8(pos), uint8(0)), uint8(254));
            b4 = uint8(bitand(bitxor(b2, b3), uint8(hex2dec('7f'))));
            fwrite(obj.ser, [b1 b2 b3 b4]);
            %fread(obj.ser,2);
        end
        
         function pos = serpos(obj, pantilt)
            b1 = uint8(hex2dec('ff'));
            b2 = uint8(uint8(160) + uint8(pantilt));
            b3 = uint8(0);
            b4 = uint8(bitand(bitxor(b2, b3), uint8(hex2dec('7f'))));
            fwrite(obj.ser, [b1 b2 b3 b4]);
            data = fread(obj.ser,2);
            pos = data(2);
        end
    end
    
    methods
        %inicializuje spojeni s motory
        function obj = CamDriver(com, transform)
            obj.trans = transform;
            obj.ser = serial(com);
            obj.ser.BytesAvailableFcnMode = 'byte';
            obj.ser.BytesAvailableFcnCount = 2;
            obj.ser.BytesAvailableFcn = @obj.callback;
            set(obj.ser,'BaudRate',57600);
            fopen(obj.ser);
        end
        
        function callback(obj,s,event)
            data = fread(s,2);
            if obj.sendState == 1
                obj.readAZ = data(2);
                obj.sendState = 2;
                obj. sermove(obj.lastSpeed, 1, obj.lastEL);
            else
                obj.readEL = data(2);
                obj.sendState = 0;
            end
        end
        
        %zameri kameru na danou pozici na obrazovce
        function setTargetPos(obj,x,y,speed)
            [az, el] = obj.trans.kart2robFast(x,y);
            setTargetAng(obj,az, el, speed);
        end
        
        %zameri kameru na danou pozici na obrazovce
        function setTargetAng(obj,az,el,speed)
            az = min(152,max(89,az));
            el = min(146,max(99,el));
            obj.lastAZ = az;
            obj.lastEL = el;
            obj.lastSpeed = speed;
            if obj.sendState == 0
                obj. sermove(speed, 0, az);
                obj.sendState = 1;
            end
        end
        
        %vrati pozici v obrazovce na kterou miri kamera
        function [x,y] = getActPos(obj)
            [az,el] = obj.getActAngle();
            [x,y] = obj.trans.rob2kart(az,el);
        end
        
        %vrati polohy obou motoru v jejich prirozenych jednotkach
        function [az,el] = getActAngle(obj)
            az = obj.serpos(0);
            el = obj.serpos(1);
        end
        
%         %vrati pozici v obrazovce na kterou miri kamera
%         function [x,y] = getActPosSave(obj)
%             [az,el] = obj.getActAngleSave();
%             [x,y] = obj.trans.rob2kart(az,el);
%         end
%         
%         %vrati polohy obou motoru v jejich prirozenych jednotkach
%         function [az,el] = getActAngleSave(obj)
%             obj.ptmove(obj.lastAZ,obj.lastEL,obj.lastSpeed);
%         end
%         
        %vrati pozici v obrazovce na kterou miri kamera
        function [x,y] = getLastPos(obj)
            [az,el] = obj.getLastAngle();
            [x,y] = obj.trans.rob2kart(az,el);
        end
        
        %vrati polohy obou motoru v jejich prirozenych jednotkach
        function [az,el] = getLastAngle(obj)
            az = obj.readAZ;
            el = obj.readEL;
        end
        
        %destruktor, pri ruseni objektu v matlabu zrusi seriove spojeni
        function delete(obj)
            fclose(obj.ser);
            delete(obj.ser);
        end
    end
    
end

