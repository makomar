classdef PIDf <handle
    %PIDF Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        P;
        I;
        D;
        f;
        oldError = 0;
        valI = 0;
        valD = 0;
        time;
    end
    
    methods
        
        function obj = PIDf(P,I,D,f)
           obj.P = P; 
           obj.I = I;
           obj.D = D;
           obj.f = f;
           obj.time = tic;
        end
        
         function reset(obj)
            obj.valI = 0;
            obj.valD = 0;
            obj.time = tic;
        end
        
        function w = reg(obj,error)
            valP = obj.P*error;
            Ts = toc(obj.time);
            obj.time = tic;
            obj.valI = obj.valI + (obj.P*Ts/obj.I)*error;
            obj.valD = (obj.D/(obj.f*Ts + obj.D))*obj.valD - ((obj.P*obj.D*obj.f)/(obj.D + obj.f*Ts))*(error - obj.oldError);
            w = valP + obj.valI + obj.valD;
            obj.oldError = error;
        end
    end
    
end

