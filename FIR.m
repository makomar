classdef FIR <handle
    %FIR Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        i;
        b;
        input;
    end
    
    methods
        
        function obj = FIR(b)
            obj.i = size(b,2);
            obj.b = b;
            obj.input = zeros(1,size(b));
        end
        
        %nastavi predchozi stavy na zvolenou hodnotu
        function preset(obj,val)
            obj.i = size(obj.b,2);
            obj.input(:) = val;
        end
        
        function out = filter(obj,in)
            obj.input(obj.i) = in;
            data = obj.input(obj.i:size(obj.input,2)) .* obj.b(1: (size(obj.b,2) - obj.i + 1));
            out = sum(data);
            if obj.i ~= 1
                data = obj.input(1:obj.i-1) .* obj.b((size(obj.b,2) - obj.i + 2):size(obj.b,2));
                out = out + sum(data);
            end
            obj.i = obj.i - 1;
            if obj.i < 1
                obj.i = size(obj.b,2);
            end
        end
        
    end
    
end

