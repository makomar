classdef Detector < handle
    %DETECTOR Modul pro vyhledavani komara v snimcich zachycenych kamerou.
    
    properties (Constant)
        % Brightness Limit - Jasova hranice pro rozpoznani komara
        BRG_LIM = 180;
        % Brightness Gnat - Jasova hranice, ktera urci, zda se jedna o
        % tmavy bod
        BRG_GNAT = 70;
    end
    
    properties (GetAccess = 'private', SetAccess = 'private')
        % Komar
        Gnat;
        % Detektor
        Detect;
        % Snimek
        Shot;
        % Zamerny kriz komara
        Cross;        
    end
    
    methods (Access = private)
        function initProperties(obj, Shot)
            obj.Gnat.SIZE = 20; % Velikost komara na snimku v pixelech
            obj.Gnat.SIZE_MIN = 3; % Minimalni tolerovana velikost komara
            obj.Gnat.SIZE_MAX = 80; % Maximalni tolerovana velikost komara
            obj.Gnat.SIZE_TOL = obj.Gnat.SIZE_MAX + 10; % Tolerance velikosti komara
            
            obj.Detect.ROW = 4; % Pocet radku snimku, po kolika ma byt detekovan komar
            obj.Detect.BORDER.Lin = -204;
            obj.Detect.BORDER.Lout = -284;
            obj.Detect.BORDER.Rin = 55;
            obj.Detect.BORDER.Rout = 126;
            obj.Detect.BORDER.Uin = -121;
            obj.Detect.BORDER.Uout = -193;
            obj.Detect.BORDER.Din = 80;
            obj.Detect.BORDER.Dout = 119;
            
            obj.Detect.BORDER.diffLoutLin = obj.Detect.BORDER.Lout - obj.Detect.BORDER.Lin;
            obj.Detect.BORDER.diffRoutRin = obj.Detect.BORDER.Rout - obj.Detect.BORDER.Rin;
            obj.Detect.BORDER.diffUoutUin = obj.Detect.BORDER.Uout - obj.Detect.BORDER.Uin;
            obj.Detect.BORDER.diffDoutDin = obj.Detect.BORDER.Dout - obj.Detect.BORDER.Din;
            
            obj.Shot = Shot; % Parametry snimku
            obj.Shot.HALF_WIDTH = Shot.WIDTH/2;
            obj.Shot.HALF_HEIGHT = Shot.HEIGHT/2;
            
            obj.Cross.SIZE = 20;
            obj.Cross.HALF_SIZE = round(obj.Cross.SIZE/2);
            obj.Cross.QUARTER_SIZE = round(obj.Cross.SIZE/4);
        end

        % Nalezeni nejtmavsiho bodu v snimku "shot" a vraceni v podobe
        % souradnic "PointDark". Pokud bude nalezeny bod tmavsi nez
        % stanovena hranice jasu, tak "PointDark.exist" bude true, v
        % opacnem pripade bude false.
        function [PointDark] = detectDark(obj, shot)
            PointDark.exist = false;
            PointDark.x = 0;
            PointDark.y = 0;
            
            [mins,idx] = min(shot);
            [Brightness,PointDark.x] = min(mins);
            PointDark.y = idx(PointDark.x);
            
            if Brightness <= obj.BRG_GNAT
                PointDark.exist = true;
            end
        end
        
        function [PointGnat, Dct] = detectGnat01Sub01(obj, shot, PointGnat, Dct)
            Dct.GnatHeight = 0;
            Dct.PointStartY.exist = false;
            Dct.PointEndY.exist = false;

            c = Dct.PointStartX.x +...
                round((Dct.PointEndX.x - Dct.PointStartX.x)/2);
            rF = Dct.PointStartX.y - obj.Gnat.SIZE_TOL;
            if rF < 1
                rF = 1;
            end
            rI = Dct.PointStartX.y + obj.Gnat.SIZE_TOL;
            if rI > obj.Shot.HEIGHT
                rI = obj.Shot.HEIGHT;
            end
            for r = rF:rI
                if (shot(r,c) <= obj.BRG_LIM) && (~Dct.PointStartY.exist)
                    tempR = r - 1;
                    if tempR >= 1
                        if (shot(tempR,c) > obj.BRG_LIM)
                            Dct.PointStartY.exist = true;
                            Dct.PointStartY.x = c;
                            Dct.PointStartY.y = r;
                        end
                    end
                elseif (Dct.PointStartY.exist) &&...
                        (shot(r,c) > obj.BRG_LIM)
                    if (Dct.GnatHeight >= obj.Gnat.SIZE_MIN) &&...
                            (Dct.GnatHeight <= obj.Gnat.SIZE_MAX)
                        Dct.PointEndY.exist = true;
                        Dct.PointEndY.x = c;
                        Dct.PointEndY.y = r;
                        break;
                    else
                        Dct.PointStartY.exist = false;
                        Dct.GnatHeight = 0;
                    end
                end
                if Dct.PointStartY.exist && (~Dct.PointEndY.exist)
                    Dct.GnatHeight = Dct.GnatHeight + 1;
                end
            end
            if Dct.PointEndY.exist
                PointGnat.exist = true;
                PointGnat.x = c;
                PointGnat.y = Dct.PointStartY.y +...
                    round((Dct.PointEndY.y - Dct.PointStartY.y)/2);
            else
                Dct.GnatWidth = 0;
                Dct.PointStartX.exist = false;
                Dct.PointEndX.exist = false;
            end
        end
        
        function [PointGnat, Dct] = detectGnat01Sub02(obj, shot, PointGnat, Dct)
            Dct.GnatWidth = 0;
            Dct.PointStartX.exist = false;
            Dct.PointEndX.exist = false;

            r = Dct.PointStartY.y +...
                round((Dct.PointEndY.y - Dct.PointStartY.y)/2);
            cF = Dct.PointStartY.x - obj.Gnat.SIZE_TOL;
            if cF < 1
                cF = 1;
            end
            cI = Dct.PointStartY.x + obj.Gnat.SIZE_TOL;
            if cI > obj.Shot.WIDTH
                cI = obj.Shot.WIDTH;
            end
            for c = cF:cI
                if (shot(r,c) <= obj.BRG_LIM) && (~Dct.PointStartX.exist)
                    tempC = c - 1;
                    if tempC >= 1
                        if (shot(r,tempC) > obj.BRG_LIM)
                            Dct.PointStartX.exist = true;
                            Dct.PointStartX.x = c;
                            Dct.PointStartX.y = r;
                        end
                    end
                elseif (Dct.PointStartX.exist) &&...
                        (shot(r,c) > obj.BRG_LIM)
                    if (Dct.GnatWidth >= obj.Gnat.SIZE_MIN) &&...
                            (Dct.GnatWidth <= obj.Gnat.SIZE_MAX)
                        Dct.PointEndX.exist = true;
                        Dct.PointEndX.x = c;
                        Dct.PointEndX.y = r;
                        break;
                    else
                        Dct.PointStartX.exist = false;
                        Dct.GnatWidth = 0;
                    end
                end
                if Dct.PointStartX.exist && (~Dct.PointEndX.exist)
                    Dct.GnatWidth = Dct.GnatWidth + 1;
                end
            end
            if Dct.PointEndX.exist
                PointGnat.exist = true;
                PointGnat.x = Dct.PointStartX.x +...
                    round((Dct.PointEndX.x - Dct.PointStartX.x)/2);
                PointGnat.y = r;
            else
                Dct.GnatHeight = 0;
                Dct.PointStartY.exist = false;
                Dct.PointEndY.exist = false;
            end
        end
        
        function [PointGnat] = detectGnat01Sub03(obj, shot, PointGnat)
            if shot(PointGnat.y,PointGnat.x) > obj.BRG_GNAT
                PointGnat.exist = false;
            end
        end
        
        % Vyhledani komara v snimku "shot" pomoci metody prohledavani po
        % radcich a sloupcich v blizkosti odhadovaneho bodu
        % "PointEstimation". Pokud "PointEstimation" neexistuje, pak se
        % prohledava cely snimek.
        function [PointGnat] = detectGnat01(obj, shot, varargin)
            optargin = size(varargin,2);
            
            PointGnat.exist = false;
            PointGnat.x = 0;
            PointGnat.y = 0;
            
            Dct.GnatWidth = 0;
            Dct.GnatHeight = 0;
            Dct.PointStartX.exist = false;
            Dct.PointEndX.exist = false;
            Dct.PointStartY.exist = false;
            Dct.PointEndY.exist = false;
            
            if optargin == 0
                rowFrom = 1;
	            rowInto = obj.Shot.HEIGHT;
	            colFrom = 1;
	            colInto = obj.Shot.WIDTH;
            elseif optargin == 1
                PointEstimation = varargin{1};
                
                if PointEstimation.exist
                    rowFrom = PointEstimation.y - obj.Gnat.SIZE_TOL;
                    if rowFrom < 1
                        rowFrom = 1;
                    end
                    rowInto = PointEstimation.y + obj.Gnat.SIZE_TOL;
                    if rowInto > obj.Shot.HEIGHT
                        rowInto = obj.Shot.HEIGHT;
                    end
                    colFrom = PointEstimation.x - obj.Gnat.SIZE_TOL;
                    if colFrom < 1
                        colFrom = 1;
                    end
                    colInto = PointEstimation.x + obj.Gnat.SIZE_TOL;
                    if colInto > obj.Shot.WIDTH
                        colInto = obj.Shot.WIDTH;
                    end
                else
                    return;
                end
            else
                fprintf('Detector - neznamy pocet parametru')
                return;
            end
            
            for row = rowFrom:obj.Detect.ROW:rowInto
                Dct.GnatWidth = 0;
                Dct.GnatHeight = 0;
                Dct.PointStartX.exist = false;
                Dct.PointEndX.exist = false;
                Dct.PointStartY.exist = false;
                Dct.PointEndY.exist = false;

                for col = colFrom:colInto
                    if (shot(row,col) <= obj.BRG_LIM) && (~Dct.PointStartX.exist)
                        tempCol = col - 1;
                        if tempCol >= 1
                            if (shot(row,tempCol) > obj.BRG_LIM)
                                Dct.PointStartX.exist = true;
                                Dct.PointStartX.x = col;
                                Dct.PointStartX.y = row;
                                %fprintf('PSX: %d %d\n',Dct.PointStartX.x,Dct.PointStartX.y)
                            end
                        end
                    elseif (Dct.PointStartX.exist) &&...
                            (shot(row,col) > obj.BRG_LIM)
                        
                        if (Dct.GnatWidth >= obj.Gnat.SIZE_MIN) &&...
                                (Dct.GnatWidth <= obj.Gnat.SIZE_MAX)
                            
                            Dct.PointEndX.exist = true;
                            Dct.PointEndX.x = col;
                            Dct.PointEndX.y = row;
                            
                            fprintf('');
                            
                            [PointGnat, Dct] = obj.detectGnat01Sub01(shot, PointGnat, Dct);
                            
                            if PointGnat.exist
                                [PointGnat, Dct] = obj.detectGnat01Sub02(shot, PointGnat, Dct);
                                [PointGnat] = detectGnat01Sub03(obj, shot, PointGnat);
                            end
                            
                            if PointGnat.exist
                                break;
                            end
                        else
                            Dct.PointStartX.exist = false;
                            Dct.GnatWidth = 0;
                        end
                    end
                    if Dct.PointStartX.exist && (~Dct.PointEndX.exist)
                        Dct.GnatWidth = Dct.GnatWidth + 1;
                    end
                end
                if PointGnat.exist
                    break;
                end
            end
        end
        
        function [Border] = detectBorder(obj,x,y)
            Border.L = 1;
            Border.R = obj.Shot.WIDTH;
            Border.U = 1;
            Border.D = obj.Shot.HEIGHT;
            
            if x < obj.Detect.BORDER.Lin
                Border.L = (x - obj.Detect.BORDER.Lin)/...
                    obj.Detect.BORDER.diffLoutLin * obj.Shot.HALF_WIDTH;
            elseif x > obj.Detect.BORDER.Rin
                Border.R = obj.Shot.WIDTH - ((x - obj.Detect.BORDER.Rin)/...
                    obj.Detect.BORDER.diffRoutRin * obj.Shot.HALF_WIDTH);
            end
            
            if y < obj.Detect.BORDER.Uin
                Border.U = (y - obj.Detect.BORDER.Uin)/...
                    obj.Detect.BORDER.diffUoutUin * obj.Shot.HALF_HEIGHT;
            elseif y > obj.Detect.BORDER.Din
                Border.D = obj.Shot.HEIGHT - ((y - obj.Detect.BORDER.Din)/...
                    obj.Detect.BORDER.diffDoutDin * obj.Shot.HALF_HEIGHT);
            end
        end
    end
    
    methods
        function obj = Detector()
            
        end
        
        function initDetector(obj,Shot)
            obj.initProperties(Shot);
        end
        
        function [PointGnat] = detection(obj, shot)
            PointGnat.exist = false;
            PointEstimation = obj.detectDark(shot);
            if PointEstimation.exist
                PointGnat = obj.detectGnat01(shot, PointEstimation);
                if ~PointGnat.exist
                    PointGnat = obj.detectGnat01(shot);
                end
            end
        end
        
        function [PointGnat] = detection1(obj, shot)
            PointGnat.exist = false;
            PointEstimation = obj.detectDark(shot);
            if PointEstimation.exist
                PointGnat = obj.detectGnat01(shot);
            end
        end
        
        function [PointGnat] = detection2(obj, shot)
            PointGnat.exist = false;
            PointGnat = obj.detectGnat01(shot);
        end
        
        % Vykresli do vlozeneho snimku "shot" kriz ukazujici na pozici
        % "Point" a pak vrati tento snimek "shot".
        function [shot] = paintCross(obj, shot, Point)
            if ~Point.exist
                return;
            end
            
            crs.xFrom = Point.x - obj.Cross.HALF_SIZE;
            crs.xInto = Point.x + obj.Cross.HALF_SIZE;
            crs.yFrom = Point.y - obj.Cross.HALF_SIZE;
            crs.yInto = Point.y + obj.Cross.HALF_SIZE;
            
            % Hlavni kriz
            for x = crs.xFrom:(crs.xFrom+obj.Cross.QUARTER_SIZE)
                if (x >= 1) && (x <= obj.Shot.WIDTH)
                    shot(Point.y,x) = 255;
                    if (Point.y+1 <= obj.Shot.HEIGHT)
                        shot(Point.y+1,x) = 0;
                    end
                end
            end
            for x = (crs.xInto-obj.Cross.QUARTER_SIZE):crs.xInto
                if (x >= 1) && (x <= obj.Shot.WIDTH)
                    shot(Point.y,x) = 255;
                    if (Point.y+1 <= obj.Shot.HEIGHT)
                        shot(Point.y+1,x) = 0;
                    end
                end
            end
            for y = crs.yFrom:(crs.yFrom+obj.Cross.QUARTER_SIZE)
                if (y >= 1) && (y <= obj.Shot.HEIGHT)
                    shot(y,Point.x) = 255;
                    if (Point.x+1 <= obj.Shot.WIDTH)
                        shot(y,Point.x+1) = 0;
                    end
                end
            end
            for y = (crs.yInto-obj.Cross.QUARTER_SIZE):crs.yInto
                if (y >= 1) && (y <= obj.Shot.HEIGHT)
                    shot(y,Point.x) = 255;
                    if (Point.x+1 <= obj.Shot.WIDTH)
                        shot(y,Point.x+1) = 0;
                    end
                end
            end
            
            % Okraje krize
            for x = (crs.xFrom+obj.Cross.QUARTER_SIZE):...
                    (crs.xInto-obj.Cross.QUARTER_SIZE)
                if (x >= 1) && (x <= obj.Shot.WIDTH) && (crs.yFrom >= 1)
                    shot(crs.yFrom,x) = 255;
                    shot(crs.yFrom+1,x) = 0;
                end
            end
            for x = (crs.xFrom+obj.Cross.QUARTER_SIZE):...
                    (crs.xInto-obj.Cross.QUARTER_SIZE)
                if (x >= 1) && (x <= obj.Shot.WIDTH) && (crs.yInto <= obj.Shot.HEIGHT)
                    shot(crs.yInto,x) = 255;
                    shot(crs.yInto-1,x) = 0;
                end
            end
            for y = (crs.yFrom+obj.Cross.QUARTER_SIZE):...
                    (crs.yInto-obj.Cross.QUARTER_SIZE)
                if (y >= 1) && (y <= obj.Shot.HEIGHT) && (crs.xFrom >= 1)
                    shot(y,crs.xFrom) = 255;
                    shot(y,crs.xFrom+1) = 0;
                end
            end
            for y = (crs.yFrom+obj.Cross.QUARTER_SIZE):...
                    (crs.yInto-obj.Cross.QUARTER_SIZE)
                if (y >= 1) && (y <= obj.Shot.HEIGHT) && (crs.xInto <= obj.Shot.WIDTH)
                    shot(y,crs.xInto) = 255;
                    shot(y,crs.xInto-1) = 0;
                end
            end
        end
        
        %% Ladici funkce
        function Border = dB(obj,x,y)
            Border = obj.detectBorder(x,y);
        end
        
    end
end