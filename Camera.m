classdef Camera
    %CAMERA Modul pro praci s USB kamerou. Zachycovani snimku a jejich
    %uprava.
    
    properties (Constant)
        
    end
    
    properties (SetAccess = private)
        vid; %odkaz na kameru
    end
    
    methods (Access = private)
        
        % Spusti nekonecne snimani kamery
        function vidStream(obj, fgi)
            set(obj.vid,'TriggerRepeat',Inf);
            obj.vid.FrameGrabInterval = fgi;
            start(obj.vid);
        end
        
        % Debayerizace obrazu z kamery (z formatu RAW na RGB)
        function [im_debay] = debay(obj,im)
            im_debay(:,:,1) = im(2:2:end,1:2:end);
            im_debay(:,:,2) = im(1:2:end,1:2:end)/2 + im(2:2:end,2:2:end)/2;
            im_debay(:,:,3) = 6.5 * im(1:2:end,2:2:end);
        end
    end
    
    methods
        % Konstruktor objektu pro praci s kamerou
        function obj = Camera()
            obj.vid = videoinput('winvideo',1,'Y800_1280x960');
        end
        
        % Spusti nekonecne snimani kamery
        function start(obj, fgi)
            komarevaluate(obj.vid);
            obj.vidStream(fgi);
        end
        
        % Z bufferu kamery precte snimek ve formatu RAW a vrati ho jako RGB
        function [Img] = getImg(obj)
            imRAW = getdata(obj.vid,1);
            imRGB = obj.debay(imRAW);
            Img = rgb2gray(imRGB);
        end
        
        % Z bufferu kamery precte snimek ve formatu RAW a vrati ho jako RGB
        function [Img] = getLastImg(obj)
            imRAW = peekdata(obj.vid,1);
            imRGB = obj.debay(imRAW);
            Img = rgb2gray(imRGB);
        end
        
        % Z bufferu kamery precte snimek ve formatu RAW a vrati ho jako RGB
        function flush(obj)
            flushdata(obj.vid);
        end
        
        % Destruktor, pri ruseni objektu v matlabu zrusi spojeni s kamerou
        function delete(obj)
            komarstop_fcn();
            stop(obj.vid);
            delete(obj.vid);
        end
    end
end