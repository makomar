classdef Transform <handle
    %TRANSFORM Modul pro prevod mezi souradnicemi v rovine obrazovky a uhlz
    %jednotlivych motoru
    
    properties (Constant)
        ElevHeight = -62; %a vyska elevacni osy
        ElCamAxisDist = 62; %c vertikalni vzdalenost mezi elevacni a kamerovou osou
        AzElDist = 31; % b bocni vzdalenost mezi osami
        FocusOffset = 22; %d pozice ohniska kamery ve smeru "vpred vzad"
        DisplayPosition = 540; %vzdalenost displeje od podstavce kamery, muze byt libovolne, jen pro vypocty
        Inc2Rad = -0.654*pi/180;        %konstanta pro prevod z jednotek senzoru na radianyo
        Pic2RadX = -4.5e-4;%-4.5e-4;       %konstanta pro prevod z pixelu kamery na odchylkovy uhel
        Pic2RadY = -4.4e-4;       %konstanta pro prevod z pixelu kamery na odchylkovy uhel
        xDisplayOffset = 0;%158; %posun pocatku souradnic displeje v ose X
        yDisplayOffset = 0;%150; %posun pocatku souradnic displeje v ose Y
    end
    
    properties (SetAccess = private)
        %model pro zamerovani cilove pozice
        aimRobot;
        %model pro urceni pozice komara v prostoru
        camRobot;
    end
    
    methods
        %vytvori objekt pro transformaci souradnic
        function obj = Transform()
            L1 = link([pi/2,0,0,obj.ElevHeight]);
            L2 = link([0,obj.ElCamAxisDist,0,-obj.AzElDist]);
            L6 = link([pi/2,obj.ElCamAxisDist,0,-obj.AzElDist]);
            L3 = link([pi/2,0,0,0]);
            L4 = link([pi/2,0,0,0]);
            L5 = link([0,0,pi,0, 1]);
            obj.aimRobot = robot({L1,L6,L5,L4,L4,L4});
            obj.camRobot = robot({L1,L2,L3,L4,L5});
        end
        
        %prevede kartezske souradnice na hodnoty pro nastaveni kamery
        %vystupni hodnoty jsou primo pulsy pro nastaveni motoru
        function [az,el] = kart2robFast(obj, x, y)
             %q = ikine(obj.aimRobot, [eye(3),[obj.DisplayPosition;-x+obj.xDisplayOffset;-y+obj.yDisplayOffset];0,0,0,1],[0,pi/2,obj.DisplayPosition,0,0,0]);
             alpha1 = atan2((x + obj.xDisplayOffset + obj.AzElDist),obj.DisplayPosition);
             dx = obj.AzElDist * cos(alpha1);
             dz = obj.AzElDist * sin(alpha1);
             alpha = -atan2((x + obj.xDisplayOffset + dx),(obj.DisplayPosition - dz));
             tmp = mod(alpha,2*pi);
             if(tmp > pi) 
                 tmp = tmp - 2 * pi ;
             end
             az= (tmp/obj.Inc2Rad) + 127;
             
             L = sqrt((x + obj.xDisplayOffset + dx)^2 + (obj.DisplayPosition - dz)^2);
             beta1 = atan2((y + obj.yDisplayOffset + obj.ElevHeight + obj.ElCamAxisDist),L);
             dy = obj.ElCamAxisDist * cos(beta1);
             dz2 = obj.ElCamAxisDist * sin(beta1);
             beta = -atan2((y + obj.yDisplayOffset + obj.ElevHeight + dy),(L - dz2));
             tmp = mod(beta - 2* pi,2*pi);
             if(tmp > pi) 
                 tmp = tmp - 2 * pi;
             end
             el= (tmp/obj.Inc2Rad) + 127;
        end
        
         %prevede kartezske souradnice na hodnoty pro nastaveni kamery
        %vystupni hodnoty jsou primo pulsy pro nastaveni motoru
        function [az,el] = kart2rob(obj, x, y)
             q = ikine(obj.aimRobot, [eye(3),[obj.DisplayPosition;-x+obj.xDisplayOffset;-y+obj.yDisplayOffset];0,0,0,1],[0,pi/2,obj.DisplayPosition,0,0,0]);
             tmp = mod(q(1),2*pi);
             if(tmp > pi) 
                 tmp = tmp - 2 * pi ;
             end
             az= (tmp/obj.Inc2Rad) + 127;
             tmp = mod(q(2) - pi/2,2*pi);
             if(tmp > pi) 
                 tmp = tmp - 2 * pi;
             end
             el= (tmp/obj.Inc2Rad) + 127;
        end
        
        %prevede natoceni uhel natoceni kamery na kartezske souradnice v
        %rovine obrazovky
        %vstupem je uhel udany v hodnotach, se kterymi pracuji motory
        function [x,y] = rob2kart(obj, az, el)
            [x,y] = pict2kart(obj,az,el,0,0);
        end
        
        %Zjisti polohu objektu z danych souradnic v obraze kamery na
        %souradnice v rovine obrazovky
        %vstupem je uhel udany v hodnotach, se kterymi pracuji motory
        %souradnice jsou pocitany od strdoveho krize
        function [x,y] = pict2kart(obj, az, el, px, py)
            T = fkine(obj.camRobot, [(az - 127)*obj.Inc2Rad ,(el - 127)*obj.Inc2Rad + pi/2,(py*obj.Pic2RadY) + pi/2,(px*obj.Pic2RadX) - pi/2,0]);
            d = (obj.DisplayPosition - T(1,4))/T(1,3);
            x = (-d * T(2,3) - T(2,4)) + obj.xDisplayOffset;
            y = (-d * T(3,3) - T(3,4)) + obj.yDisplayOffset;
        end
        
        function [az,el] = pict2robAAA(obj, az, el, px, py)
            posunMax = 4;
            osaX = posunMax/320;
            osaY = posunMax/240;
            
            difAz = 0;
            difEl = 0;
            if px < 320
                difAz = -((320-px)*osaX);
            end
            if px > 320
                difAz = ((px-320)*osaX);
            end
            if py < 240
                difEl = -((240-py)*osaY);
            end
            if py > 240
                difEl = +((py-240)*osaY);
            end
            
            az = az + difAz;
            el = el + difEl;
        end

        
        %zobrazi model kamery pro zamereni bodu v prostoru v dane pozici
        %uhly jsou zadavany v radianech
        function plotAimBotAng(obj, az, el, len)
            plot(obj.aimRobot, [ az , el + pi/2,len, 0, 0, 0]);
        end
        
        %zobrazi model kamery pro urceni polohy bjektu z obrazu v dane
        %pozici
        %uhly jsou zadavany v radianech
        function plotCamBotAng(obj, az, el, len, dx, dy)
            plot(obj.camRobot, [az ,el  + pi/2,dx*obj.Pic2RadX + pi/2,dy*obj.Pic2RadY - pi/2,len]);
        end
        
        %zobrazi model kamery pro zamereni bodu v prostoru v dane pozici
        %uhly jsou zadavany v hodnotach se kterymi pracuji motory
        function plotAimBotInc(obj, az, el, len)
            obj.plotAimBotAng((az - 127)*obj.Inc2Rad ,(el - 127)*obj.Inc2Rad , len);
        end
        
         %zobrazi model kamery pro urceni polohy bjektu z obrazu v dane
         %pozici
        %uhly jsou zadavany v hodnotach se kterymi pracuji motory
        function plotCamBotInc(obj,az, el, len, dx, dy)
            obj.plotCamBot((az - 127)*obj.Inc2Rad,(el - 127)*obj.Inc2Rad,dx,dy,len);
        end
        
        
    end
    
end

