classdef Aiming < handle
    %AIMING Rezim zamerovani komara.
    
    properties (Constant)
        SHOT_WIDTH = 640; % sirka snimku
        SHOT_HEIGHT = 480; % vyska snimku
%         hranice pro omezeni rozsahu predikce
        U_outy = -193;
        L_outx = -284;
        R_outx = 126;
        D_outy = 119;
        % omezeni akcniho zasahu
        max_ux = 150;
        min_ux = -150;
        max_uy = 150;
        min_uy = -150;
        predict_time = 2;
        
        % Frame Grab Interval - Kazdy 'CAM_FGI'. snimek bude zachycen v
        % bufferu kamery (kamera zachycuje asi 18 snimku/sekundu). Tedy pro
        % 'CAM_FGI = 1;' bude zachyceno 18/1 = 18 snimku za sekundu a
        % napriklad pro 'CAM_FGI = 3;' bude zachyceno 18/3 = 6 snimku
        % za sekundu.
        CAM_FGI = 1;
    end
    
    properties (GetAccess = 'private', SetAccess = 'private')
        camera; % odkaz na objekt kamery
        camDriver; % odkaz na objekt ridici jednotky
        detector; % odkaz na objekt detekce komara
        transform; % odkaz na objekt transformaci souradnic
        predictor; % odkaz na objekt predikce pohybu komara
        regulator; % odkaz na objekt regulatoru
        
        Shot; % parametry snimku
        Cross; % parametry zamerneho krize
    end
    
    methods (Access = private)
        % Inicializace prvku tridy Aiming
        function initComponents(obj)
            obj.initShot();
            obj.initCross();
            obj.detector.initDetector(obj.Shot);
        end
        
        % Inicializace parametru snimku
        function initShot(obj)
            obj.Shot.WIDTH = obj.SHOT_WIDTH; % Sirka snimku
            obj.Shot.HEIGHT = obj.SHOT_HEIGHT; % Vyska snimku
        end
        
        % Inicializace parametru zamerneho krize
        function initCross(obj)
            obj.Cross.SIZE = 20;
            
            obj.Cross.X = round(obj.Shot.WIDTH/2);
            obj.Cross.Y = round(obj.Shot.HEIGHT/2);
            obj.Cross.X_FROM = obj.Cross.X - round(obj.Cross.SIZE/2);
            obj.Cross.X_INTO = obj.Cross.X + round(obj.Cross.SIZE/2);
            obj.Cross.Y_FROM = obj.Cross.Y - round(obj.Cross.SIZE/2);
            obj.Cross.Y_INTO = obj.Cross.Y + round(obj.Cross.SIZE/2);
        end
        
        % Funkce pro vykresleni zamerneho krize do snimku 'shot' a nasledne
        % jeho vraceni.
        function [shot] = paintCross(obj, shot)
            for x = obj.Cross.X_FROM:obj.Cross.X_INTO
                shot(obj.Cross.Y,x) = 0;
                shot(obj.Cross.Y+1,x) = 255;
            end
            for y = obj.Cross.Y_FROM:obj.Cross.Y_INTO
                shot(y,obj.Cross.X) = 0;
                shot(y,obj.Cross.X+1) = 255;
            end
        end
    end
    
    methods
        %% Konstruktor objektu pro praci s kamerou
        function obj = Aiming(camera, camDriver, detector, transform, predictor)
            obj.camera = camera;
            obj.camDriver = camDriver;
            obj.detector = detector;
            obj.transform = transform;
            obj.predictor = predictor;
            
            obj.initComponents();
        end
        %% Metody
        
        % Spusteni rezimu zamerovani na dobu definovanou poctem smycek
        %  LOOP_COUNT     pocet smycek opakovani zamerovani
        function [sequence,test,times] = start(obj, LOOP_COUNT)
            sequence = 1;%uint8(zeros(480,640,LOOP_COUNT));
            test = 1;%zeros(14,LOOP_COUNT);
            times = 1;%zeros(1,LOOP_COUNT);
            
            pidx = PIDf(0.78,6,0,30);
            pidy = PIDf(0.82,2,0,30);
            ff = [2 1.25 0.75];
            ff = ff /sum(ff);
            fx = FIR(ff);
            fy = FIR(ff);
            lastTime = tic;
            firstseen = 1;
            sx = -31;
            sy = 0;
            obj.camDriver.setTargetPos(sx,sy,1);
            x = sx;
            y = sy;
            urlread('http://147.32.84.120/komar2/komar.cgi?refresh=150&size=50&accel=300&asize=25');
            obj.camera.start(obj.CAM_FGI);
            obj.camera.flush();
            for i = 1:2
                shot = obj.camera.getImg();
            end
            
            obj.camDriver.setTargetPos(sx,sy,1); %pocatecni pozici zadam jeste jednou, aby se mi v ovladaci ulozila spravna pozice
            pause(0.1);
            time = tic;
            for i = 1:LOOP_COUNT
                PointGnat = obj.detector.detection(shot);
                [az,el] = obj.camDriver.getLastAngle();
                [actx,acty] = obj.transform.rob2kart(az,el);
                if PointGnat.exist
                    [x,y] = obj.transform.pict2kart(az,el,PointGnat.x - 320,PointGnat.y - 240);
                    if firstseen == 1
                        %pokud je toto prvni zpozorovani komara, tak
                        %predikci pridat vice hodnot najednou, aby si
                        %myslela, ze komar stoji
                        firstseen = 0;
                        obj.predictor.newData(x,y,time - 2e5);
                        obj.predictor.newData(x,y,time - 1e5);
                        fx.preset(x);
                        fy.preset(y);
                    end
                    
                    obj.predictor.newData(x,y,time);
                    t = time + obj.predict_time * (time - lastTime);
                    [px,py] = obj.predictor.predict(t);
                    px = max(obj.L_outx,min(obj.R_outx,px));
                    py = max(obj.U_outy,min(obj.D_outy,py));
                    fax = fx.filter(px);
                    fay = fy.filter(py);
                    ax = pidx.reg(fax - actx);
                    ay = pidy.reg(fay - acty);
%                     test(:,i) = [actx;acty;x;y;px;py;ax;ay;PointGnat.x - 320 ;PointGnat.y - 240;fax;fay;az;el];
                else
                    t = time + obj.predict_time *(time - lastTime);
                    [px,py] = obj.predictor.predict(t);
                    px = max(obj.L_outx,min(obj.R_outx,px));
                    py = max(obj.U_outy,min(obj.D_outy,py));
                    if  px == 0 && py == 0 && firstseen == 0
                        pidx.reset();
                        pidy.reset();
                        firstseen = 1;
                    end
                    ax = pidx.reg(px - actx);
                    ay = pidy.reg(py - acty);
%                     test(:,i) = [actx;acty;x;y;px;py;ax;ay;NaN;NaN;NaN;NaN;az;el];
                end
%                 times(1,:) = time - lastTime;
%                 shot = obj.paintCross(shot);
%                 if PointGnat.exist
%                       shot((PointGnat.y - 3):(PointGnat.y + 3 ) ,PointGnat.x ) =  255 ;
%                       shot(PointGnat.y  ,(PointGnat.x - 3 ):(PointGnat.x + 3 )) =  255 ;
%                 end
%                 sequence(:,:,i) = shot(:,:);
%                 imshow(shot)
                lastTime = time;   
                ax = max(obj.min_ux,min(obj.max_ux,ax));
                ay = max(obj.min_uy,min(obj.max_uy,ay));
                obj.camDriver.setTargetPos(actx + ax,acty + ay,3);
                toc(time)
                time = tic;
                shot = obj.camera.getImg();
                obj.camera.flush();
               
%                 pause(0.01);
            end
        end
    end
end