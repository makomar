classdef Calibration < handle
    %CALIBRATION Rezim kalibrace.
    
    properties (Constant)
        
    end
    
    properties (GetAccess = 'private', SetAccess = 'private')
        Cam; % odkaz na objekt kamery
        CamDr; % odkaz na objekt ridici jednotky
        Detect; % odkaz na objekt detekce komara
        Trans; % odkaz na objekt transformaci souradnic
        Pred; % odkaz na objekt predikce pohybu komara
        Regul; % odkaz na objekt regulatoru
    end
    
    methods (Access = private)
        
        function [shot] = initComponents(obj)
            obj.Cam.start(1);
            for i = 1:10
                obj.Cam.getImg();
            end
            shot = obj.Cam.getImg();
            imshow(shot)
            obj.Cam.delete();
            obj.Detect.initDetector(shot);
        end
        
    end
    
    methods
        %% Konstruktor objektu rezimu kalibrace
        function obj = Calibration(Camera, CamDriver, Detector, Transform)
            obj.Cam = Camera;
            obj.CamDr = CamDriver;
            obj.Detect = Detector;
            obj.Trans = Transform;
            obj.Pred = Predictor;
            obj.Regul = Regulator;
        end
        %% Metody
        
        function [shot] = start(obj)
            [shot] = obj.initComponents();
        end
        
    end
end