function komarstop_fcn
global komar_vid

if ~isempty(komar_vid) && isvalid(komar_vid) % video object exists
  stoppreview(komar_vid);
end
