function [lasttime nframes] = komarlostframes_fcn(nowtime, lasttime, framerate)

if lasttime > 0 % not the first frame
  nframes = floor(((datenum(nowtime)-lasttime)*24*3600+0.5/framerate)*framerate)-1;
else
  nframes = 0;
end

lasttime = datenum(nowtime);
