function komarclose_fcn(src, evnt)
global komar_vid komar_size komar_write
global komar_dir komar_fmt
global komar_idx komar_max komar_buffer komar_times

komarstop_fcn; % stop grabbing if running

if komar_write
  delete([komar_dir filesep '*' komar_fmt]);
else
  clear komar_buffer;
  clear komar_times;
end

delete(gcf);

clear global komar_vid;
clear global komar_size;
clear global komar_write;

clear global komar_dir;
clear global komar_fmt;

clear global komar_idx;
clear global komar_max;
clear global komar_buffer;
clear global komar_times;
