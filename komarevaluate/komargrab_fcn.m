function komargrab_fcn(obj,event,himage)
global komar_size komar_write
global komar_dir komar_fmt
global komar_idx komar_max komar_buffer komar_times

if (event.Data(1,1)>0) && (event.Data(1,2)>0) && (event.Data(2,1)>0) && (event.Data(2,2)>0) % is non-black
  im = imresize(event.Data,komar_size,'nearest');
  if komar_write
    fname = regexprep(event.Timestamp,':|\.','');
    imwrite(im,[komar_dir filesep fname komar_fmt]);
  elseif komar_idx < komar_max
    komar_idx = komar_idx+1;
    komar_buffer(:,:,komar_idx) = im;
    komar_times{komar_idx} = event.Timestamp;
  else
    fprintf(1,'KOMARgrab: Buffer full!\n');
    komarstop_fcn;
  end
  %fprintf(1,'%s\n',event.Timestamp); % write timestamps
  %set(himage, 'CData', im); % show preview
end
