function komarevaluate(vid)

global komar_vid komar_size komar_write
global komar_dir komar_fmt
global komar_idx komar_max komar_buffer komar_times
global komar_laser_sigma;
global komar_detector_scale;


komar_vid = vid;
komar_size = 0.5; % downsample images
komar_write = 0; % 0:memory, 1:file
komar_laser_sigma = 40; % variance of the laser, in pixels
komar_detector_scale = 0.5; % downscaling of the image for the reference detector

% write 1
komar_dir = 'C:\Documents and Settings\defuser';
komar_fmt = '.bmp';

% write 0
komar_idx = 0;
komar_max = 1000; % number of frames to save in memory

vidRes = get(vid, 'VideoResolution');
imWidth = vidRes(1)*komar_size;
imHeight = vidRes(2)*komar_size;
nBands = get(vid, 'NumberOfBands');

komar_buffer = uint8(zeros(imHeight, imWidth, komar_max));
komar_times = cell(1,komar_max);

hFig = figure('Toolbar','none',...
    'Menubar', 'none',...
    'NumberTitle','Off',...
    'Name','Komar grabber');
set(hFig,'CloseRequestFcn',@komarclose_fcn);

hImage = image(zeros(imHeight, imWidth, nBands));
set(hImage,'Visible','off'); % hide preview

uicontrol('String', 'Stop',...
    'Callback', 'komarstop_fcn',...
    'Units','normalized',...
    'Position',[.06 0.5 .25 0.5]);
uicontrol('String', 'Process',...
    'Callback', 'komarprocess_fcn',...
    'Units','normalized',...
    'Position',[.37 0.5 .25 0.5]);
uicontrol('String', 'Close',...
    'Callback', 'komarclose_fcn',...
    'Units','normalized',...
    'Position',[.68 0.5 .25 0.5]);
uicontrol('String', 'Stop All',...
    'Callback', 'komarstopall_fcn',...
    'Units','normalized',...
    'Position',[.06 0 .25 0.5]);

%figSize = get(hFig,'Position');
%set(gca,'unit','pixels',...
%        'position',[ ((figSize(3) - imWidth)/2)...
%                     ((figSize(4) - imHeight)/2)...
%                       imWidth imHeight ]);
set(hFig,'unit','pixels',...
    'position',[8 700 300 70]);

% Set up the update preview window function.
setappdata(hImage,'UpdatePreviewWindowFcn',@komargrab_fcn);

preview(vid, hImage);
end
