function komarprocess_fcn
global komar_write
global komar_dir komar_fmt
global komar_idx komar_buffer komar_times
global komar_laser_sigma;
global komar_detector_scale;


komarstop_fcn; % stop grabbing if running

framerate = 15;
lasttime = 0;
fig = figure;

if komar_write
  imgs = dir([komar_dir filesep '*' komar_fmt]);
  total = length(imgs);
else
  total = komar_idx;
end

info = [];
frames = [];

h = figure();

for i=1:total
  if komar_write
    im = imread([komar_dir filesep imgs(i).name]);
    nowtime = [imgs(i).name(1:2) ':' imgs(i).name(3:4) ':' imgs(i).name(5:6) '.' imgs(i).name(7:9)];
  else
    im = komar_buffer(:,:,i);
    nowtime = komar_times{i};
  end
  [lasttime nframes] = komarlostframes_fcn(nowtime,lasttime,framerate);
  if nframes > 5, fprintf(1,'%s: lost %d frames.\n',nowtime,nframes); end;
  %imshow(im); pause(1/framerate); % DO ANY PROCESSING HERE
  info = evaluate_komar_frame(info,im,komar_detector_scale,komar_laser_sigma,h);
  frames(i).detected = info.detected;
  frames(i).pos = [info.x info.y];
  frames(i).size = info.s;
  frames(i).score = info.score;
  frames(i).lostframes = nframes;
  info.Score = info.Score+info.score*(nframes);
  info.score = info.score*(1+nframes);
end
if(total>0)
	Score = info.Score;
else
Score = 0;
end
Time = total;
fprintf('Total score: %f, total frames: %i\n',Score,Time);
save([komar_dir '/_results.mat'],'frames','Score','Time');

%close(fig);