%% Priprava

clc;
clear all;
addpath('robot');
addpath('komarevaluate');

%% Konstanty

LOOP_COUNT = 800; % pocet cyklu zamerovani

%% Objekty

% Transformace souradnic
transform = Transform();
% Pripojeni ovladani Pan-Tilt jednotky (pohyb kamery)
camDriver = CamDriver('COM1', Transform);
% Pripojeni ovladani USB kamery (obraz z kamery)
camera = Camera();
% Deklarace detektoru komara (inicializace soucasti kalibrace)
detector = Detector();
% Deklarace prediktoru pohybu komara
predictor = Predictor();

%% Rezim zamerovani
aiming = Aiming(camera, camDriver, detector, transform, predictor);% ,...
    %regulator);
tic
[sequence,test,times] = aiming.start(LOOP_COUNT);
toc

%% Ukonceni

camDriver.delete();
camera.delete();

clear transform camDriver camera calibration aiming




